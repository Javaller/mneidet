package com.bignerdranch.android.lookintentet;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by Home on 18.10.2016.
 */
public class LookListFragment extends ListFragment {

    private static final String TAG = "LookListFragment";
    private ArrayList<Look> mLooks;
    private boolean mSubtitleVisible;


    @Override
    public void onResume() {
        super.onResume();
        ((LookAdapter)getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.looks_title);
        mLooks = LookLab.get(getActivity()).getLooks();
//        ArrayAdapter<Look> adapter =
//                new ArrayAdapter<Look>(getActivity(),
//                        android.R.layout.simple_list_item_1,
//                        mLooks);
        LookAdapter adapter = new LookAdapter(mLooks);
        setListAdapter(adapter);
        setRetainInstance(true);
        mSubtitleVisible = false;
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (mSubtitleVisible) {
                getActivity().getActionBar().setSubtitle(R.string.subtitle);
            }
        }

        ListView listView = (ListView)v.findViewById(android.R.id.list);
        registerForContextMenu(listView);

        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Look look = (Look) (getListAdapter()).getItem(position);
        Log.d(TAG, look.getTitle() +  " was clicked");
//        Intent i = new Intent(getActivity(), LookActivity.class);
        Intent i = new Intent(getActivity(), LookPagerActivity.class);
        i.putExtra(LookFragment.EXTRA_LOOK_ID, look.getId());
        startActivity(i);
    }

    private class LookAdapter extends ArrayAdapter<Look> {
        public LookAdapter(ArrayList<Look> looks) {
            super(getActivity(), 0, looks);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.list_item_look, null);
            }
            Look look = getItem(position);
            TextView titleTextView =
                    (TextView)convertView.findViewById(R.id.look_list_item_titleTextView);
            titleTextView.setText(look.getTitle());
            TextView dateTextView =
                    (TextView)convertView.findViewById(R.id.look_list_item_dateTextView);
            dateTextView.setText(look.getStringDate());
            CheckBox solvedCheckBox =
                    (CheckBox)convertView.findViewById(R.id.look_list_item_solvedCheckBox);
            solvedCheckBox.setChecked(look.isSolved());
            return convertView;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_look_list, menu);

//        MenuItem showSubtitle = menu.findItem(R.id.menu_item_show_subtitle);
//        if (mSubtitleVisible && showSubtitle != null) {
//            showSubtitle.setTitle(R.string.hide_subtitle);
//        }
    }

    @TargetApi(11)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_look:
                Look look = new Look();
                LookLab.get(getActivity()).addLook(look);
                Intent i = new Intent(getActivity(), LookPagerActivity.class);
                i.putExtra(LookFragment.EXTRA_LOOK_ID, look.getId());
                startActivityForResult(i, 0);
                return true;
//            case R.id.menu_item_show_subtitle:
//                if (getActivity().getActionBar().getSubtitle() == null) {
//                    getActivity().getActionBar().setSubtitle(R.string.subtitle);
//                    mSubtitleVisible = true;
//                    item.setTitle(R.string.hide_subtitle);
//                }
//                else {
//                    getActivity().getActionBar().setSubtitle(null);
//                    mSubtitleVisible = false;
//                    item.setTitle(R.string.show_subtitle);
//                }
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        getActivity().getMenuInflater().inflate(R.menu.look_list_item_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int position = info.position;
        LookAdapter adapter = (LookAdapter)getListAdapter();
        Look look = adapter.getItem(position);
        switch (item.getItemId()) {
            case R.id.menu_item_delete_look:
                LookLab.get(getActivity()).deleteLook(look);
                adapter.notifyDataSetChanged();
                return true;
        }
        return super.onContextItemSelected(item);
    }
}
