package com.bignerdranch.android.lookintentet;

import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;


import java.util.UUID;

public class LookActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {

        UUID lookId = (UUID)getIntent()
                .getSerializableExtra(LookFragment.EXTRA_LOOK_ID);
        return LookFragment.newInstance(lookId);
    }
}
