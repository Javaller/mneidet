package com.bignerdranch.android.lookintentet;

import android.support.v4.app.ListFragment;
import android.support.v4.app.Fragment;

/**
 * Created by Home on 18.10.2016.
 */
public class LookListActivity extends SingleFragmentActivity {

    protected ListFragment createFragment()
    {
        return new LookListFragment();
    }
}