package com.bignerdranch.android.lookintentet;

/**
 * Created by Home on 18.10.2016.
 */
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Date;
import java.util.UUID;


public class LookFragment extends Fragment {
    private Look mLook;
    private EditText mTitleField;
    private Button mDateButton;
    private Button mConsultantButton;

    private CheckBox mSolvedCheckBox;
    public static final String EXTRA_LOOK_ID =
            "com.bignerdranch.android.lookintentet.look_id";
    private static final String DIALOG_DATE = "date";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_PHOTO = 1;
    private static final int REQUEST_CONTACT = 2;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private static final String TAG = "LookFragment";
    private static final String DIALOG_IMAGE = "image";


    public void updateDate() {
        mDateButton.setText(mLook.getStringDate());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID lookId = (UUID)getActivity().getIntent()
                .getSerializableExtra(EXTRA_LOOK_ID);
        mLook = LookLab.get(getActivity()).getLook(lookId);
//        mLook = new Look();
        setHasOptionsMenu(true);
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_look, parent, false);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
//        }

        mTitleField = (EditText)v.findViewById(R.id.look_title);
        mTitleField.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(
                    CharSequence c, int start, int before, int count) {
                mLook.setTitle(c.toString());
            }
            public void beforeTextChanged(
                    CharSequence c, int start, int count, int after) {
                // Здесь намеренно оставлено пустое место
            }
            public void afterTextChanged(Editable c) {
                // И здесь тоже
            }
        });

        mPhotoView = (ImageView)v.findViewById(R.id.look_imageView);
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Photo p = mLook.getPhoto();
                if (p == null)
                    return;
                android.app.FragmentManager fm = getActivity().getFragmentManager();
//                FragmentManager fm = getActivity()
//                        .getSupportFragmentManager();
                String path = getActivity()
                        .getFileStreamPath(p.getFilename()).getAbsolutePath();
                ImageFragment.newInstance(path).show(fm, DIALOG_IMAGE);
            }
        });


        mDateButton = (Button)v.findViewById(R.id.look_date);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                //DatePickerFragment dialog = new DatePickerFragment();
                DatePickerFragment dialog = DatePickerFragment
                        .newInstance(mLook.getDate());
                dialog.setTargetFragment(LookFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });

        mSolvedCheckBox = (CheckBox)v.findViewById(R.id.solved);
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                // Назначение флага раскрытия преступления
                mLook.setSolved(isChecked);
            }
        });

        mPhotoButton = (ImageButton)v.findViewById(R.id.look_imageButton);
        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LookCameraActivity.class);
                startActivityForResult(i, REQUEST_PHOTO);
            }
        });

        PackageManager pm = getActivity().getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) &&
                !pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            mPhotoButton.setEnabled(false);
        }

        Button reportButton = (Button)v.findViewById(R.id.reportButton);
        reportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                i.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.look_report_subject));
                i = Intent.createChooser(i, getString(R.string.send_report));
                startActivity(i);
            }
        });

        mConsultantButton = (Button) v.findViewById(R.id.consultantButton);
        mConsultantButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
            }
        });
        if(mLook.getConsultant() != null) {
            mConsultantButton.setText(mLook.getConsultant());
        }

        return v;
    }

    private void showPhoto() {
        Photo p = mLook.getPhoto();
        BitmapDrawable b = null;
        if (p != null) {
            String path = getActivity()
                    .getFileStreamPath(p.getFilename()).getAbsolutePath();
            b = PictureUtils.getScaledDrawable(getActivity(), path);
        }
        mPhotoView.setImageDrawable(b);
    }

    @Override
    public void onStart() {
        super.onStart();
        showPhoto();
    }

    @Override
    public void onStop() {
        super.onStop();
        PictureUtils.cleanImageView(mPhotoView);
    }

    public static LookFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_LOOK_ID, crimeId);
        LookFragment fragment = new LookFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_DATE) {
            Date date = (Date)data
                    .getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mLook.setDate(date);
            updateDate();
        } else if (requestCode == REQUEST_PHOTO) {
            String filename = data
                    .getStringExtra(LookCameraFragment.EXTRA_PHOTO_FILENAME);
            if (filename != null) {
                Photo p = new Photo(filename);
                mLook.setPhoto(p);
                showPhoto();
            }
        } else if (requestCode == REQUEST_CONTACT) {
            Uri contactUri = data.getData();
            String[] queryFields = new String[] {
                    ContactsContract.Contacts.DISPLAY_NAME
            };
            Cursor c = getActivity().getContentResolver()
                    .query(contactUri, queryFields, null, null, null);
            if (c.getCount() == 0) {
                c.close();
                return;
            }
            c.moveToFirst();
            String suspect = c.getString(0);
            mLook.setConsultant(suspect);
            mConsultantButton.setText(suspect);
            c.close();
        }
        else {return;}
    }

    @Override
    public void onPause() {
        super.onPause();
        LookLab.get(getActivity()).saveLooks();
    }

    private String getCrimeReport() {
        String solvedString = null;
        if (mLook.isSolved()) {
            solvedString = getString(R.string.look_report_solved);
        } else {
            solvedString = getString(R.string.look_report_unsolved);
        }
        String dateFormat = "EEE, MMM dd";
        String dateString = DateFormat.format(dateFormat, mLook.getDate()).toString();
        String suspect = mLook.getConsultant();
        if (suspect == null) {
            suspect = getString(R.string.look_report_no_suspect);
        } else {
            suspect = getString(R.string.look_report_suspect, suspect);
        }
        String report = getString(R.string.look_report,
                mLook.getTitle(), dateString, solvedString, suspect);
        return report;
    }
}