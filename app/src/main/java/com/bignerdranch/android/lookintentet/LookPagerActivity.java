package com.bignerdranch.android.lookintentet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Home on 19.10.2016.
 */
public class LookPagerActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private ArrayList<Look> mLooks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        mLooks = LookLab.get(this).getLooks();
        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public int getCount() {
                return mLooks.size();
            }
            @Override
            public Fragment getItem(int pos) {
                Look look = mLooks.get(pos);
                return LookFragment.newInstance(look.getId());
            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) { }
            public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {
            }
            public void onPageSelected(int pos) {
                Look look = mLooks.get(pos);
                if (look.getTitle() != null) {
                    setTitle(look.getTitle());
                }
            }
        });

        UUID crimeId = (UUID)getIntent()
                .getSerializableExtra(LookFragment.EXTRA_LOOK_ID);
        for (int i = 0; i < mLooks.size(); i++) {
            if (mLooks.get(i).getId().equals(crimeId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}