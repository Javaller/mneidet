package com.bignerdranch.android.lookintentet;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by VOpolski on 25.10.2016.
 */

public class LookIntentJSONSerializer {
    private Context mContext;
    private String mFilename;
    public LookIntentJSONSerializer(Context c, String f) {
        mContext = c;
        mFilename = f;
    }

    public ArrayList<Look> loadLooks() throws IOException, JSONException {
        ArrayList<Look> looks = new ArrayList<Look>();
        BufferedReader reader = null;
        try {
            InputStream in = mContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                jsonString.append(line);
            }
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString())
                    .nextValue();
            for (int i = 0; i < array.length(); i++) {
                looks.add(new Look(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
// Происходит при начале "с нуля"; не обращайте внимания
        } finally {
            if (reader != null)
                reader.close();
        }
        return looks;
    }

    public void saveLooks(ArrayList<Look> looks)
            throws JSONException, IOException {
// Построение массива в JSON
        JSONArray array = new JSONArray();
        for (Look l : looks)
            array.put(l.toJSON());
// Запись файла на диск
        Writer writer = null;
        try {
            OutputStream out = mContext
                    .openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
