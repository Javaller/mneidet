package com.bignerdranch.android.lookintentet;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Home on 18.10.2016.
 */
public class LookLab {

    private static final String TAG = "LookLab";
    private static final String FILENAME = "clooks.json";

    private ArrayList<Look> mLooks;
    private LookIntentJSONSerializer mSerializer;

    private static LookLab sLookLab;
    private Context mAppContext;

    private LookLab(Context appContext) {
        mAppContext = appContext;
        mSerializer = new LookIntentJSONSerializer(mAppContext, FILENAME);
//        mLooks = new ArrayList<Look>();
//        for (int i = 0; i < 100; i++) {
//            Look c = new Look();
//            c.setTitle("Look #" + i);
//            c.setSolved(i % 2 == 0); // Для каждого второго объекта
//            mLooks.add(c);
//        }
        try {
            mLooks = mSerializer.loadLooks();
        } catch (Exception e) {
            mLooks = new ArrayList<Look>();
            Log.e(TAG, "Error loading crimes: ", e);
        }
    }

    public static LookLab get(Context c) {
        if (sLookLab == null) {
            sLookLab = new LookLab(c.getApplicationContext());
        }
        return sLookLab;
    }

    public void addLook(Look l) {
        mLooks.add(l);
    }

    public void deleteLook(Look l) {
        mLooks.remove(l);
        saveLooks();
    }

    public ArrayList<Look> getLooks() {
        return mLooks;
    }

    public Look getLook(UUID id) {
        for (Look c : mLooks) {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }

    public boolean saveLooks() {
        try {
            mSerializer.saveLooks(mLooks);
            Log.d(TAG, "crimes saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving crimes: ", e);
            return false;
        }
    }




}
